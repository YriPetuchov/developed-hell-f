![alt text][logo]

[logo]: https://pp.userapi.com/c841221/v841221651/714db/JvBjEcO94js.jpg "Logo WTH"

---

# DeveloperHell

Организатор, онлайн энциклопедия, социальная сеть, предоставляющая своим пользователям возможность формирования списка своей активности над контентом, просмотр списков других пользователей, формирование личных рекомендаций. Предоставление такой информации как кадры, трейлеры, постеры, обои, а также о личностях, связанных с контентом. Сортировка и каталогизации контента, с возможностью редактирования пользователем.


> Наша цель – сделать гибкий, функциональный и прогрессивный ресурс, полезный большинству современных человеков.

> WTH был создан без поддержки сторонних лиц, и является самостоятельным openssource проектом.

> Команда WTH была основана в 2018 году.

###### Этот проект был сгенерирован [Angular CLI](https://github.com/angular/angular-cli) version 1.6.5.

---

## Команды

Собираем проект - `ng build --prod --aot`

Устанавливаем пакеты - `npm install`

Запуск сервера - `ng serve` для сервера dev.

Навигация - `http://localhost:4200/`. Приложение будет автоматически перезагружаться, если вы измените любой из исходных файлов.

---

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

*Just for fun*